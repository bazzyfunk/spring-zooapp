package com.crea.dev1.springbootproject.zoo.dao;

import java.util.List;

import com.crea.dev1.springbootproject.zoo.model.Mammal;

import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * MammalDAO
 */
public interface MammalDAO extends ElasticsearchRepository<Mammal, Integer> {

    public Page<Mammal> findAll();

    public Mammal findById(int id);

    List<Mammal> findByAgeGreaterThan(int ageLimit);

    Mammal findByNameLike(String name);
}