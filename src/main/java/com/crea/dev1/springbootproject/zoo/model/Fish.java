package com.crea.dev1.springbootproject.zoo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity

public class Fish {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    int id;

    private String name;
    private Float age;
    private String description;

    public Fish() {

    }

    public Fish(int id, String name, Float age, String description) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.description = description;
    }

    /**
     * @return int return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return String return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        if (name != null) {
            this.name = name;
        }
    }

    /**
     * @return Integer return the age
     */
    public Float getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(Float age) {
        this.age = age;
    }

    /**
     * @return String return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

}