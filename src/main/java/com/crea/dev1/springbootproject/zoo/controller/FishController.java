package com.crea.dev1.springbootproject.zoo.controller;

import java.util.List;

import com.crea.dev1.springbootproject.zoo.dao.FishDAO;
import com.crea.dev1.springbootproject.zoo.model.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import io.swagger.annotations.Api;

@Api(description = "Api pour les optération CRUD sur les produits")
@CrossOrigin(origins = "http://localhost:8080", maxAge = 3600)
@RestController("/fishs")
public class FishController {
    @Autowired
    private FishDAO fishDAO;

    public RestTemplate rest() {
        return new RestTemplate();
    }

    @GetMapping("/fishs")
    public List<Fish> getAllFishs() {
        List<Fish> myFishs = fishDAO.findAll();
        return myFishs;
    }

    @PostMapping(path = "/fishs/add")
    public List<Fish> addFish(@RequestBody Fish fish) {
        // code
        fishDAO.save(fish);
        return fishDAO.findAll();
    }

    @PutMapping(path = "/fishs/update")
    public Fish updateFish(@RequestBody Fish fish) {
        fishDAO.save(fish);
        return fishDAO.findById(fish.getId());
    }

    @DeleteMapping(path = "/fishs/delete/{id}")
    public List<Fish> deleteFish(@PathVariable int id) {
        fishDAO.deleteById(id);
        return fishDAO.findAll();
    }

}