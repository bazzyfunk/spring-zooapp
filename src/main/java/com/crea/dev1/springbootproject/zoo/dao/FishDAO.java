package com.crea.dev1.springbootproject.zoo.dao;

import java.util.List;

import com.crea.dev1.springbootproject.zoo.model.Fish;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FishDAO extends JpaRepository<Fish, Integer> {

    public List<Fish> findAll();

    public Fish findById(int id);

    public Fish findByNameLike(String name);

}