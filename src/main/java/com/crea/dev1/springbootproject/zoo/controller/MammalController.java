package com.crea.dev1.springbootproject.zoo.controller;

import com.crea.dev1.springbootproject.zoo.dao.MammalDAO;
// import com.example.demo.Exception.ProductIntrouvable;
import com.crea.dev1.springbootproject.zoo.model.Mammal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

/**
 * MammalController
 */
@Api(description = "API pour les operations CRUD sur les heros.")
@RestController
public class MammalController {

    @Autowired
    private MammalDAO MammalDAO;

    // @RequestMapping(value = "/Mammals", method = RequestMethod.GET)
    // public String listeMammals() {
    // return "Un example de hero";
    // }

    @RequestMapping(value = "/Mammals", method = RequestMethod.GET)
    public Page<Mammal> listeMammals() {
        return MammalDAO.findAll();
    }

    @RequestMapping(value = "/MammalsGetByName/{name}")
    public Mammal afficherUnMamifereParNom(@PathVariable String name) {
        return MammalDAO.findByNameLike(name);
    }

    @RequestMapping(value = "/addMammal", method = RequestMethod.POST)
    public Mammal addNew(@RequestBody Mammal mammal) {
        return MammalDAO.save(mammal);
    }

    @RequestMapping(value = "/updateMammal/{id}", method = RequestMethod.PUT)
    public Mammal updateNew(@RequestBody Mammal mammal) {
        return MammalDAO.save(mammal);
    }

    @RequestMapping(value = "/deleteMammal/{id}", method = RequestMethod.DELETE)
    public Page<Mammal> DeleteUnMamifereSpecific(@PathVariable int id) {
        MammalDAO.deleteById(id);
        return MammalDAO.findAll();
    }

    // @ApiOperation(value = "Chercher un produits specific et donner son prix en
    // chf et dollar.")
    // @RequestMapping(value = "/Mammals/{id}")
    // public String afficherUnHeroSpecific(@PathVariable int id) {
    // Mammal heroId = MammalDAO.findById(id);
    // String convertedPrice = getConvertedAge(heroId.getAge());

    // // if (heroId == null) {
    // // throw new HeroIntrouvable("Mammal with ID " + id + " not found");
    // // }

    // return convertedPrice;

    // }

    // @RequestMapping(value = "/MammalsGetAge/{ageLimit}")
    // public List<Mammal> afficherHeroByPrixGreaterThan(@PathVariable int
    // ageLimit) {
    // return MammalDAO.seekforHighestAge(ageLimit);
    // }
}